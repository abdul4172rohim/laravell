@extends('layout.data-tables')

@section('judul')
Halaman Home
@endsection

@section('content')
<h1>Media Online</h1>
    <h2>Sosial Media Developer</h2>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik<p>
    <h2>Benefit join di Media Online</h2>
    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li> <br>
        <li>Sharing knowledge</li> <br>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h2>Cara Bergabung ke Media Online</h2>
    <ol>
        <li>Mengunjungi website ini</li> <br>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a> </li> <br>
        <li>Selesai</li>
    
    </ol>
@endsection
