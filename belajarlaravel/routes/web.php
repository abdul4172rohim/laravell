<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'utama'])->name('home');
Route::get('/register', [AuthController::class, 'register'])->name('register');

Route::post('/welcome', [AuthController::class, 'welcome'])->name('welcome');

Route::get('/data-tables', function(){
    return view('page.datatable');
});